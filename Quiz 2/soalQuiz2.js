// Soal 1 (CLASS)

class Score {
    constructor(subject, points, email) {
        this.subject = subject
        this.email = email
        this.points = points
    }

}

class Count extends Score {
    constructor(points) {
        super(points);
        this.points = points;
    }
    average() {
        var currentAverage = 0;
        var total = 0;
        if (this.points.length > 1) {
            this.points.forEach(value => {
                total += value;
            });
            currentAverage = total / this.points.length;
            return currentAverage;
        } else {
            currentAverage = this.points;
            return currentAverage;
        }
    }
}
var count = new Score("Hitung Average", [10, 20, 30], "a@email.com");
console.log(count.email);
console.log(count.points);
console.log(count.subject);
var countIt = new Count([10, 20, 30])
console.log(countIt.average());

// Soal 2 (Create)
var dataToShow = "";
const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

var objectData = {};
function viewScores(data, subject) {
    for (let i = 0; i < data.length; i++){
        if (subject == "quiz-1"){
            objectData.email = data[i][0];
            objectData.subject = subject;
            objectData.points = data[i][1];
        } else if (subject == "quiz-2"){
            objectData.email = data[i][0];
            objectData.subject = subject;
            objectData.points = data[i][2];
            // dataToShow += "email: " + data[i][0] + ", subject: " + subject + ", points: " + data[i][2];
        } else {
            objectData.email = data[i][0];
            objectData.subject = subject;
            objectData.points = data[i][3];
            // dataToShow += "email: " + data[i][0] + ", subject: " + subject + ", points: " + data[i][3];
        }
    }
    console.log(objectData);
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")
