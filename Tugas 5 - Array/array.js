// Soal 1 (Range)
var numbers = [];
function range(startNum, finishNum) {
    if (startNum == null || finishNum == null){
        numbers.splice(0, numbers.length);
        numbers.push(-1);
        return numbers;
    } else {
        if (startNum < finishNum) {
            numbers.splice(0, numbers.length);
            while (startNum <= finishNum) {
                numbers.push(startNum);
                startNum++;
            }
            return numbers;
        } else {
            numbers.splice(0, numbers.length);
            while (startNum >= finishNum) {
                numbers.push(startNum);
                startNum--;
            }
            return numbers;
        }
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal 2 (Range with Step)
var numbers2 = [];
function rangeWithStep(startNum, finishNum, step) {
    if (startNum < finishNum) {
        numbers2.splice(0, numbers2.length);
        while (startNum <= finishNum) {
            numbers2.push(startNum);
            startNum += step;
        }
        return numbers2;
    } else {
        numbers2.splice(0, numbers2.length);
        while (startNum >= finishNum) {
            numbers2.push(startNum);
            startNum -= step;
        }
        return numbers2;
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

// Soal 3 (Sum of Range)
var numbers3 = [];
var total = 0;
function sum(startNum, finishNum = 0, step = 1) {
    if (startNum < finishNum) {
        total = 0;
        while (startNum <= finishNum) {
            total += startNum;
            startNum += step;
        }
        return total;
    } else {
        total = 0;
        while (startNum >= finishNum) {
            total += startNum;
            startNum -= step;
        }
        return total;
    }
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal 4 (Array Multidimensi)
var data ="";
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
function dataHandling(input) {
    for (var i = 0; i < input.length; i++){
        data += "Nomor ID: " + input[i][0] + "\n";
        data += "Nama Lengkap: " + input[i][1] + "\n";
        data += "TTL: " + input[i][2] + " " + input[i][3] + "\n";
        data += "Hobi: " + input[i][4] + "\n";
        data += "\n";
    }
    return data;
}

console.log(dataHandling(input));

// Soal 5 (Balik Kata)
var currentString = '';
var newString = '';
function balikKata(kata){
    currentString = kata;
    newString = '';
    for(var i = kata.length - 1; i >= 0; i--) {
        newString = newString + currentString[i];
    }
    return newString;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal 6 (Metode Array)
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
var tanggal;
function dataHandling2(data){
    data[1] = data[1] + "Elsharawy";
    data[2] = "Provinsi " + data[2];
    data.splice(4, 1, "Pria", "SMA Internasional Metro");
    tanggal = data[3].split("/");

    console.log(data);
    switch (tanggal[1]) {
        case "01": {
            console.log("Januari");
            break;
        }
        case "02": {
            console.log("Februari");
            break;
        }
        case "03": {
            console.log("Maret");
            break;
        }
        case "04": {
            console.log("April");
            break;
        }
        case "05": {
            console.log("Mei");
            break;
        }
        case "06": {
            console.log("Juni");
            break;
        }
        case "07": {
            console.log("Juli");
            break;
        }
        case "08": {
            console.log("Agustus");
            break;
        }
        case "09": {
            console.log("September");
            break;
        }
        case "10": {
            console.log("Oktober");
            break;
        }
        case "11": {
            console.log("November");
            break;
        }
        case "12": {
            console.log("Desember");
            break;
        }
    }

    // Sorting
    tanggal.sort(function (value1, value2) {
        return value2 - value1;
    });
    console.log(tanggal); 
    
    // Join
    console.log(data[3].split("/").join("-"))

    // Slice
    console.log(data[1].slice(0, 14)); //[1, 2, 3]

}

dataHandling2(input);
