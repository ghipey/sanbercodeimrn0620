// If-else
var nama = "Junaedi"
var peran = "Werewolf"

if (nama == "" && peran == ""){
    console.log("Nama harus diisi!");
} else if (nama != "" && peran == ""){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
} else if (nama != "" && peran == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, ".concat(nama));
    console.log("Halo ".concat(peran + " ").concat(nama + ", kamu dapat melihat siapa yang menjadi werewolf!"));
} else if (nama != "" && peran == "Guard") {
    console.log("Selamat datang di Dunia Werewolf, ".concat(nama));
    console.log("Halo ".concat(peran + " ").concat(nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf."));
} else if (nama != "" && peran == "Werewolf") {
    console.log("Selamat datang di Dunia Werewolf, ".concat(nama));
    console.log("Halo ".concat(peran + " ").concat(nama + ", Kamu akan memakan mangsa setiap malam!"));
}

// Switch case
var tanggal = 16; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 6; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2100; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

if (tanggal > 0 && tanggal < 32){
    if (tahun >= 1900 && tahun <=2200){
        if (bulan > 0 && bulan < 13){
            switch (bulan) {
                case 1: {
                    console.log(tanggal + " " + "Januari " + tahun);
                    break;
                }
                case 2: {
                    console.log(tanggal + " " + "Februari " + tahun);
                    break;
                }
                case 3: {
                    console.log(tanggal + " " + "Maret " + tahun);
                    break;
                }
                case 4: {
                    console.log(tanggal + " " + "April " + tahun);
                    break;
                }
                case 5: {
                    console.log(tanggal + " " + "Mei " + tahun);
                    break;
                }
                case 6: {
                    console.log(tanggal + " " + "Juni " + tahun);
                    break;
                }
                case 7: {
                    console.log(tanggal + " " + "Juli " + tahun);
                    break;
                }
                case 8: {
                    console.log(tanggal + " " + "Agustus " + tahun);
                    break;
                }
                case 9: {
                    console.log(tanggal + " " + "September " + tahun);
                    break;
                }
                case 10: {
                    console.log(tanggal + " " + "Oktober " + tahun);
                    break;
                }
                case 11: {
                    console.log(tanggal + " " + "November " + tahun);
                    break;
                }
                case 12: {
                    console.log(tanggal + " " + "Desember " + tahun);
                    break;
                }
            }
        } else {
            console.log("Masukkan bulan sesuai range yang ditentukan");
        }
    } else {
        console.log("Masukkan tahun sesuai range yang ditentukan");
    }
} else {
    console.log("Masukkan tanggal sesuai range yang ditentukan");
}