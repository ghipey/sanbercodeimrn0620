// Soal 1 (Array to Object)
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)
var personObj = {};
function arrayToObject(arr) {
    if (arr.length == 0){
        console.log('"');
    } else {
        for (var i = 0; i < arr.length; i++) {
            personObj.firstName = arr[i][0]
            personObj.lastName = arr[i][1]
            personObj.gender = arr[i][2]
            if (arr[i][3] == null) {
                personObj.age = "Invalid birth year"
            } else {
                if (arr[i][3] > thisYear) {
                    personObj.age = "Invalid birth year"
                } else {
                    personObj.age = thisYear - arr[i][3]
                }
            }
            console.log(i+1 + ". " + arr[i][0] + " " + arr[i][1] + ":", JSON.stringify(personObj))
        }
    }

}

// Driver Code
var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

// Soal 2 (Shopping Time)
var saleObject = {};
var changeMoney = 0;

function shoppingTime(memberId, money) {
    var listPurchased = [];
    if (memberId == null || memberId == ''){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else {
        if (money < 50000){
            return "Mohon maaf, uang tidak cukup";
        } else {
            changeMoney = money;
            for (i = 0; i < 5; i++) {
                if (changeMoney >= 1500000) {
                    changeMoney -= 1500000;
                    listPurchased.push('Sepatu Stacattu');
                } else if (changeMoney >= 500000) {
                    changeMoney -= 500000;
                    listPurchased.push('Baju Zoro');
                } else if (changeMoney >= 250000) {
                    changeMoney -= 250000;
                    listPurchased.push('Baju H&N');
                } else if (changeMoney >= 175000) {
                    changeMoney -= 175000;
                    listPurchased.push('Sweater Uniklooh');
                } else if (changeMoney >= 50000) {
                    changeMoney -= 50000;
                    listPurchased.push('Casing Handphone');
                }
            }
            saleObject.memberId = memberId;
            saleObject.money = money;
            saleObject.listPurchased = listPurchased;
            saleObject.changeMoney = changeMoney;
            return saleObject
        }
    }
    
}

// // TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal 3 (Naik Angkot)
var jalur = {};
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    // variabel angkot sebagai penampung hasil
    var angkot = [{}, {}];
    var asal = '';
    var tujuan = '';

    for (var i = 0; i < arrPenumpang.length; i++) {
        var j = 0;

        for (j; j < arrPenumpang[i].length; j++) {
            switch (j) {
                case 0: {
                    angkot[i].penumpang = arrPenumpang[i][j];
                    break;
                }
                case 1: {
                    angkot[i].naikDari = arrPenumpang[i][j];
                    angkot[i].tujuan = arrPenumpang[i][j + 1];
                    break;
                }
                case 2: {
                    asal = arrPenumpang[i][j - 1];
                    tujuan = arrPenumpang[i][j];
                    var jarak = 0;
                    for (var k = 0; k < rute.length; k++) {
                        if (rute[k] === asal) {
                            for (var l = k + 1; l < rute.length; l++) {
                                jarak += 1;
                                if (rute[l] === tujuan) {
                                    var bayar = jarak * 2000;
                                    angkot[i].bayar = bayar;
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
    }
    return angkot;
}

//TEST CASE
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]