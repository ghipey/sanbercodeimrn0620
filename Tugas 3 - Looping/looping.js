// Soal 1 Looping While
var i = 2;
var j = 20;
while(i < 21){
    if(i == 2){
        console.log("LOOPING PERTAMA");
    }
    console.log(i + " - I love coding");
    if (i == 20) {
        console.log("LOOPING KEDUA");
        while (j > 1) {
            console.log(j + " - I will become a mobile developer");
            j -= 2;
        }
    }
    i += 2;
}

// Soal 2 Looping menggunakan for
console.log("OUTPUT");
for (var k = 1; k < 21; k++){
    if (k % 2 == 0){
        console.log(k + " - Berkualitas");
    } else {
        if (k % 3 == 0){
            console.log(k + " - I Love Coding");
        } else {
            console.log(k + " - Santai");
        }
    }
}

// Soal 3 Membuat Persegi Panjang 
var persegiPanjang = "";
for (var i = 0; i < 4; i++){
    for (var j = 0; j < 8; j++){
        persegiPanjang += "#";
    }
    persegiPanjang += "\n";
}
console.log(persegiPanjang);

// Soal 4 Membuat Tangga
var tangga = ""; 
for (var i = 1; i <= 7; i++) {
    for (var j = i; j >= 1; j--) {
        tangga += "#";
    }
    tangga += "\n";
}
console.log(tangga);

// Soal 5 Membuat Papan Catur
var papanCatur = "";
for (var i = 0; i < 8; i++) { // buat vertikal
    for (var j = 0; j < 8; j++) { // buat horizontal
        if (i % 2 != 0) { // setiap i bertemu bilangan ganjil
            if (j % 2 == 0) { // setiap j bertemu bilangan genap
                papanCatur += "#"; // cetak pagar '#'
            } else { // setiap j bertemu bilangan ganjil
                papanCatur += " "; // cetak spasi
            }

        } else { // setiap i bertemu bilangan genap polanya diganti 
            if (j % 2 == 0) { // setiap j bertemu bilangan genap
                papanCatur += " "; // cetak spasi
            } else { // setiap j bertemu bilangan ganjil
                papanCatur += "#"; // cetak pagar '#'
            }
        }
    }
    papanCatur += "\n";
}
console.log(papanCatur);