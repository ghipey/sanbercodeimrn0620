// Soal A (Bandingkan Angka)
function bandingkan(num1 = 0, num2 = 0) {
    if (num1 < 0 || num2 < 0){
        return -1;
    } else {
        if (num1 == num2) {
            return -1;
        } else {
            if (num1 > num2){
                return num1;
            } else {
                return num2;
            }
        }
    }  
}
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121)); // 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18

// Soal B (Balik String)
var currentString = '';
var newString = '';
function balikString(kata) {
    currentString = kata;
    newString = '';
    for (var i = kata.length - 1; i >= 0; i--) {
        newString = newString + currentString[i];
    }
    return newString;
}

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

// Soal C (Palindrome)
var currentString = '';
var newString = '';
function palindrome(kata) {
    currentString = kata;
    newString = '';
    for (var i = kata.length - 1; i >= 0; i--) {
        newString = newString + currentString[i];
    }
    
    if (newString == currentString){
        return true;
    } else {
        return false;
    }
}

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false