// Soal A (Descending Ten)
var deret = [];
function DescendingTen(num) {
    if (num == null){
        return -1;
    } else {
        deret = [];
        for (var i = num; i > num - 10; i--) {
            deret.push(i);
        }
        var array = deret.join(" ");
        return array;
    }
    
}

console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

// Soal B (Ascending Ten)
var deretAscending = [];
function AscendingTen(num) {
    if (num == null) {
        return -1;
    } else {
        deretAscending = [];
        for (var i = num; i < num + 10; i++) {
            deretAscending.push(i);
        }
        var arrayAscending = deretAscending.join(" ");
        return arrayAscending;
    }
}

console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

// Soal C (Conditional Ascending Descending)
var deretC = [];
var arrayC;
function ConditionalAscDesc(reference, check) {
    if (reference == null || check == null) {
        return -1;
    } else {
        if (check % 2 != 0){
            deretC = [];
            for (var i = reference; i < reference + 10; i++) {
                deretC.push(i);
            }
            arrayC = deretC.join(" ");
            return arrayC;
        } else {
            deretC = [];
            for (var i = reference; i > reference - 10; i--) {
                deretC.push(i);
            }
            arrayC = deretC.join(" ");
            return arrayC;
        }
    }
}

console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

// Soal D (Papan Ular Tangga)
var tangga = "";
function ularTangga() {
    for (var i = 100; i > 9; i -= 10) {
        if ((i/2)%2 == 0){
            for (var j = i; j > i - 10; j--) {
                tangga += j + " ";
            }
            tangga += "\n";
        } else {
            for (var j = i-9; j <= i; j++) {
                tangga += j + " ";
            }
            tangga += "\n";
        }
    }
    return tangga;
}
console.log(ularTangga());