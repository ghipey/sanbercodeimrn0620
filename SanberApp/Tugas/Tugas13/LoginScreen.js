import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class LoginScreen extends Component {

    render() {
    
        return (
            <View style={styles.container}>
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Icon style={styles.titleIcon} name="home" color={"#20639B"} size={240}/>
                </View>
                <Text style={styles.titleText}>Sanber Community App</Text>
                <TextInput style={styles.inputBorder} marginTop={52}
                    // onChangeText={text => onChangeText(text)}
                    // value={"Username"}
                    placeholder="Username/Email"
                    placeholderTextColor = "#7A9DBA"
                />
                <TextInput style={styles.inputBorder} marginTop={19}
                    // onChangeText={text => onChangeText(text)}
                    // value={"Username"}
                    placeholder="Password"
                    placeholderTextColor = "#7A9DBA"
                />
                <TouchableOpacity TouchableOpacity style = {styles.submitButton}>
                    <Text style={styles.submitText} textAlign={"center"}>LOGIN</Text>
                </TouchableOpacity>
                <Text style={styles.regisText}>Belum punya akun? Daftar disini . . .</Text>
            </View>
        );
    
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    titleText: {
        alignItems: "center",
        flexDirection: "column",
        fontSize: 24,
        fontWeight: "bold",
        justifyContent: "center",
        color: "#20639B",
        marginTop: -36
    },
    inputText: {
        fontSize: 14,
        color: "#7A9DBA"
    },
    inputBorder: {
        borderRadius: 16,
        borderWidth: 4,
        borderColor: "#20639B",
        backgroundColor: "#FFFFFF",
        paddingTop: 12,
        paddingBottom: 12,
        textAlign: "center"
    },
    submitButton: {
        borderColor: "#FFFFFF",
        borderRadius: 16,
        borderWidth: 4,
        backgroundColor: "#20639B",
        justifyContent: "center",
        marginTop: 19,
        paddingTop: 12,
        paddingBottom: 12,
    },
    submitText: {
        fontSize: 14,
        color: "#FFFFFF",
        textAlign: "center"
    },
    regisText: {
        alignItems: "center",
        color: "#8F8E8E",
        flexDirection: "column",
        fontSize: 14,
        fontStyle: "italic",
        justifyContent: "center",
        marginTop: 11,
        textAlign: "center"

    },
    titleIcon: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 80
    }
});


