import React, {
    Component
} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TextInput,
    Dimensions
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class AboutScreen extends Component {

    render() {
    
        return (
            <View style={styles.container}>
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Icon style={styles.titleIcon} name="account-circle" color={"#FFFFFF"} size={240}/>
                </View>
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={styles.titleText}>Nama Lengkap</Text>
                </View>
                <View View style = {{alignItems: 'flex-start', justifyContent: 'center', backgroundColor: 'white',
                    borderRadius: 20, marginEnd: 80, marginStart: 80, marginTop: 19, paddingTop: 4, paddingBottom: 4,
                    paddingStart: 16}}>
                    <Text style={styles.itemTextTitle}>Sosial Media</Text>
                    <View View style = {{alignItems: "flex-start", justifyContent: "center",flexDirection: "row", paddingStart: 16, marginTop: 3}}>
                        <Icon style={styles.itemIcon} name="facebook-box" color={"#20639B"} size={22}/>
                        <Text style={styles.itemText}>Akun facebook</Text>
                    </View>
                    <View View style = {{alignItems: "flex-start", justifyContent: "center",flexDirection: "row", paddingStart: 16, marginTop: 3}}>
                        <Icon style={styles.itemIcon} name="twitter" color={"#20639B"} size={22}/>
                        <Text style={styles.itemText}>Akun twitter</Text>
                    </View>
                    <View View style = {{alignItems: "flex-start", justifyContent: "center",flexDirection: "row", paddingStart: 16, marginTop: 3}}>
                        <Icon style={styles.itemIcon} name="instagram" color={"#000000"} size={22}/>
                        <Text style={styles.itemText}>Akun instagram</Text>
                    </View>
                </View>

                <View View style = {{alignItems: 'flex-start', justifyContent: 'center', backgroundColor: 'white',
                    borderRadius: 20, marginEnd: 80, marginStart: 80, marginTop: 22, paddingTop: 4, paddingBottom: 4,
                    paddingStart: 16}}>
                    <Text style={styles.itemTextTitle}>Portofolio</Text>
                    <View View style = {{alignItems: "flex-start", justifyContent: "center",flexDirection: "row", paddingStart: 16, marginTop: 3}}>
                        <Icon style={styles.itemIcon} name="github-circle" color={"#000000"} size={22}/>
                        <Text style={styles.itemText}>http://github.com/nama</Text>
                    </View>
                    <View View style = {{alignItems: "flex-start", justifyContent: "center",flexDirection: "row", paddingStart: 16, marginTop: 3}}>
                        <Icon style={styles.itemIcon} name="gitlab" color={"#000000"} size={22}/>
                        <Text Text style = {styles.itemText}>http://gitlab.com/123</Text>
                    </View>
                    <View View style = {{alignItems: "flex-start", justifyContent: "center",flexDirection: "row", paddingStart: 38, marginTop: 3}}>
                        <Text Text style = {styles.itemText}>http://project-ku/123</Text>
                    </View>
                </View>
                <TouchableOpacity TouchableOpacity style = {styles.submitButton}>
                    <Text style={styles.submitText} textAlign={"center"}>EDIT PROFIL</Text>
                </TouchableOpacity>
            </View>
        );
    
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#20639B",
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },
    titleText: {
        alignItems: "center",
        flexDirection: "column",
        fontSize: 24,
        fontWeight: "bold",
        justifyContent: "center",
        color: "#FFFFFF",
        marginTop: -12
    },
    itemTextTitle: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#20639B"
    },
    itemText: {
        fontSize: 14,
        fontWeight: "bold",
        color: "#20639B",
        marginStart: 4,
        marginTop: 1
    },
    submitButton: {
        borderColor: "#FFFFFF",
        borderRadius: 20,
        backgroundColor: "#FFFFFF",
        justifyContent: "center",
        marginEnd: 60,
        marginStart: 60,
        marginTop: 22,
        paddingTop: 14,
        paddingBottom: 14,
    },
    submitText: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#20639B",
        textAlign: "center"
    },
    regisText: {
        alignItems: "center",
        color: "#8F8E8E",
        flexDirection: "column",
        fontSize: 14,
        fontStyle: "italic",
        justifyContent: "center",
        marginTop: 11,
        textAlign: "center"

    },
    titleIcon: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 56
    },
    itemIcon: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    }
});



