var readBooksPromise = require('./promise.js')

var books = [{
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    }
]

function read(times, index) {
    if (index < books.length){
        readBooksPromise(times, books[index])
            .then(function (sisaWaktu) {
                return read(sisaWaktu, index + 1);
            })
            .catch(function (error) {
                console.log(error.message);
            });
    }
    
}

// Tanya Mom untuk menagih janji
read(10000, 0)