// di index.js
var readBooks = require('./callback.js')

var books = [{
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    }
]

var time = 10000;

function read(times, index) {
    if (index < books.length) {
        readBooks(times, books[index], function(sisaWaktu){
            return read(sisaWaktu, index + 1);
        })
    } else {
        return;
    }
}

read(time, 0);